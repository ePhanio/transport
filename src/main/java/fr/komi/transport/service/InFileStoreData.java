package fr.komi.transport.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.komi.transport.model.Train;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.prefs.Preferences;

@Service
@ConditionalOnProperty(value = "storage_mode", havingValue = "file")
public class InFileStoreData implements StoreData  {

    @Value("${file_path}")
    String storePath;
    ObjectMapper objectMapper = new ObjectMapper();
    Map trains = new HashMap();

    @Override
    public Train store(Train train) {
        try {
           // objectMapper.writeValue(Files.newBufferedWriter(storePath, StandardOpenOption.APPEND), train);
            // Create JSON
            final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(train);

// Content is appended (due to StandardOpenOption.APPEND)
            Files.write(new File(storePath).toPath(), Arrays.asList(json), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Map load() {
        try {
            //comment recuperer la liste?
          Train  t = objectMapper.readValue(new File(storePath), Train.class);
          trains.put(t.getIdentify(),t);
          return trains;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Train getTrainById(Integer id) {
        this.load();
        if(isTrainExist(id)){
         return (Train) trains.get(id);
        }
        return null;
    }

    @Override
    public boolean isTrainExist(Integer identify) {
        return  trains.get(identify)!=null;
    }

    @Override
    public Train editTrain(Train train) {
        this.load();
       if(isTrainExist(train.getIdentify())){
           //comment faire l'update dans le fichier

          return (Train) trains.put(train.getIdentify(),train);
       }
       return null;
    }

    @Override
    public boolean removeTrain(Train train) {
        return false;
    }

    @Override
    public boolean removeTrain(Integer indentify) {
        return false;
    }
}
