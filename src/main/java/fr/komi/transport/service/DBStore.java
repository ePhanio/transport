package fr.komi.transport.service;

import fr.komi.transport.model.Train;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@ConditionalOnProperty(value = "storage_mode", havingValue = "dbase")
public class DBStore implements StoreData {
    @Override
    public Train store(Train train) {
        return null;
    }

    @Override
    public Map<Integer, Train> load() {
        return null;
    }

    @Override
    public Train getTrainById(Integer id) {
        return null;
    }

    @Override
    public boolean isTrainExist(Integer identify) {
        return false;
    }

    @Override
    public Train editTrain(Train train) {
        return null;
    }

    @Override
    public boolean removeTrain(Train train) {
        return false;
    }

    @Override
    public boolean removeTrain(Integer indentify) {
        return false;
    }
}
