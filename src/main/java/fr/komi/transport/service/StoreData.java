package fr.komi.transport.service;

import fr.komi.transport.model.Train;

import java.util.List;
import java.util.Map;

public interface StoreData {
    /**
     * Store list of train
     * @param trais
     */
    public Train store(Train train);

    /**
     * load list of train
     * @return
     */
    public Map<Integer,Train> load();

    /**
     * get train by id
     * @param id
     * @return
     */
    public Train getTrainById(Integer id);

    /**
     * verify is train exist
     * @param identify
     * @return
     */
    public  boolean isTrainExist(Integer identify);

    /**
     * edit object train
     * @param train
     * @return
     */
    public Train editTrain(Train train);

    /**
     * remove the train by object
     * @param train
     * @return
     */
    public abstract boolean removeTrain(Train train);

    /**
     * remove the train by id
     * @param indentify
     * @return
     */
    public abstract boolean removeTrain(Integer indentify);

}
