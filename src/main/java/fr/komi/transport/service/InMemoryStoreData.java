package fr.komi.transport.service;

import fr.komi.transport.model.Train;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@ConditionalOnProperty(value = "storage_mode", havingValue = "memory", matchIfMissing = true)
public class InMemoryStoreData implements StoreData {
    private static Map trains = new HashMap();
    @Override
    public Train store(Train train) {
        if(!this.isTrainExist(train.getIdentify())){
           return (Train) this.trains.put(train.getIdentify(), train);
        }
        return null;
    }

    @Override
    public Map load() {
        return trains;
    }

    @Override
    public Train getTrainById(Integer id) {
        return (Train) trains.get(id);
    }

    @Override
    public boolean isTrainExist(Integer identify) {
        return trains.containsKey(identify);
    }

    @Override
    public Train editTrain(Train train) {
        if(this.isTrainExist(train.getIdentify()))
           return (Train) trains.put(train.getIdentify(),train);
        return null;
    }

    @Override
    public boolean removeTrain(Train train) {
      return this.removeTrain(train.getIdentify());
    }

    @Override
    public boolean removeTrain(Integer identify) {
       if(this.isTrainExist(identify)){
           trains.remove(identify);
           return true;
       }
        return false;
    }
}
