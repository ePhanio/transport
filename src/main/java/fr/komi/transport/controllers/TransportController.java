package fr.komi.transport.controllers;

import fr.komi.transport.dao.SNCFApiDAO;
import fr.komi.transport.model.DataSNCF;
import fr.komi.transport.model.Train;
import fr.komi.transport.service.StoreData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TransportController {

    @Autowired
    private StoreData storeService;

    @Autowired
    private SNCFApiDAO sncfApiDAO;

    @GetMapping("/")
    public Map<Integer,Train> getTrains() {
        return storeService.load();
    }

    @PostMapping("/train")
    public void addTrain(@RequestBody Train train){
        storeService.store(train);
    }

    @PutMapping("/train/{id}")
    public void edit(@RequestBody Train train){
        storeService.editTrain(train);
    }

    @GetMapping("/train/{id}")
    public Train getTrain(@PathVariable("id") Integer id){
        return storeService.getTrainById(id);
    }

    @DeleteMapping("/train/{id}")
    public Boolean removeTrain(@PathVariable("id") Integer id){
        return storeService.removeTrain(id);
    }




    @GetMapping("/api_sncf")
    public DataSNCF listTrainSNCF() {
        return sncfApiDAO.fetchData();
    }
}
