package fr.komi.transport.model;

public class DataSNCF {

    private  Pagination pagination;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {

        this.pagination = pagination;
    }
}
