package fr.komi.transport.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pagination {

    @JsonProperty("start_page")
    private int startPage;
    @JsonProperty("items_per_page")
    private int itemsPerPage;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
}
