package fr.komi.transport.dao;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.komi.transport.model.DataSNCF;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
public class SNCFApiDAO {

    public DataSNCF fetchData() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200917T144459&")
                .header("Authorization","Basic MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .build();

        try {
            Response response = client.newCall(request).execute();
            System.out.println(response.code());
            String data = response.body().string();
            JsonNode node = new ObjectMapper().readTree(data);
            System.out.println(node.get("pagination").get("items_on_page").asInt());

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            DataSNCF dataSNCF = objectMapper.readValue(data, DataSNCF.class);
         //   System.out.println(dataSNCF.getPagination().getItemsPerPage());
            return dataSNCF;
        }
        catch (IOException e) {
            System.out.println("Erreur réseau");
            e.printStackTrace();
        }

        return null;
    }
}
