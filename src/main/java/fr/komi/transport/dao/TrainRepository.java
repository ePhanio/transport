package fr.komi.transport.dao;

import fr.komi.transport.model.Train;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TrainRepository implements JpaRepository<Train, Integer> {
    @Override
    public List<Train> findAll() {
        return null;
    }

    @Override
    public List<Train> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Train> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Train> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(Train train) {

    }

    @Override
    public void deleteAll(Iterable<? extends Train> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Train> S save(S s) {
        return null;
    }

    @Override
    public <S extends Train> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Train> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Train> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<Train> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Train getOne(Integer integer) {
        return null;
    }

    @Override
    public <S extends Train> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Train> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Train> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Train> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Train> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Train> boolean exists(Example<S> example) {
        return false;
    }
}
